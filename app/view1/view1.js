'use strict';

angular.module('myApp.view1', ['ngRoute'])

   .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/home', {
         templateUrl: 'view1/view1.html',
         controller: 'View1Ctrl'
      });
   }])

   .controller('View1Ctrl', [function () {
      /*backendService.api({method: 'GET', url: 'worlds'}, function successCallback(response) {
         console.log(response);
      }, function errorCallback(response) {
         console.error(response);
         // errback
      });*/
   }]);