/**
 * Created by zr.fayzrahmanov on 04.12.2017.
 */

(function () {
   'use strict';
   angular.module('myApp')
      .directive('userList', [
         function () {
            return {
               restrict: 'EA',
               css: 'components/userList/userList.css',
               controller: 'userListCtrl',
               scope: {
                  ngModel: "=",
                  title: "@",
                  type: "@",
                  options: "@"
               },
               templateUrl: 'components/userList/userList.html'
            };
         }
      ])
      .controller('userListCtrl', [
         '$scope',
         function ($scope) {
            $scope.selectedView='list'; // по дефолту будет отображение списком
            $scope.tabs=[
               {
                  value:'list',
                  caption: "Списком"
               },
               {
                  value:'tiles',
                  caption: "Плиткой"
               },
               {
                  value:'byGroups',
                  caption: "По группам"
               }
            ]; // Вкладки с вариантами отображения списка пользователей

            $scope.list = [
               {
                  fio:'vasya',
                  company: 'company1',
                  group: 'group1',
                  phone:'1234123',
                  email:'mail@mail.ru'
               },
               {
                  fio:'vasya2',
                  company: 'company1',
                  group: 'group2',
                  phone:'1234123',
                  email:'mail@mail.ru'
               },
               {
                  fio:'vasya3',
                  company: 'company1',
                  group: 'group2',
                  phone:'1234123',
                  email:'mail@mail.ru'
               },
               {
                  fio:'vasya4',
                  company: 'company1',
                  group: 'group3',
                  phone:'1234123',
                  email:'mail@mail.ru'
               },
               {
                  fio:'vasya5',
                  company: 'company1',
                  group: 'group3',
                  phone:'1234123',
                  email:'mail@mail.ru'
               },
               {
                  fio:'vasya6',
                  company: 'company1',
                  group: 'group2',
                  phone:'1234123',
                  email:'mail@mail.ru'
               },
               {
                  fio:'vasya7',
                  company: 'company1',
                  group: 'group1',
                  phone:'1234123',
                  email:'mail@mail.ru'
               }
            ];

            $scope.someText = 'azaza!!!1';
            $scope.changeView = function () {
               // setView()
               // тут инициализируем нужную вкладку
               console.log(this.selectedView);
            };
         }
      ]);
})();