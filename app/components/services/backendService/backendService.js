/**
 * Created by fr0zen on 04.12.2017.
 */
(function () {
    'use strict';
    angular.module('myApp')
        .factory('backendService', [
            '$http',
            function ($http) {
                var backendService = {
                    options: {
                        url: 'http://site.com:4000/' // сюда впихнем урл нашего сервера где крутится бекенд, пока не готов, на днях залью
                    },
                    api: function (params, callback, errback) {
                        $http({
                            method: params.method,
                            url: backendService.options.url+params.url,
                            dataType: 'jsonp'
                        }).then(callback, errback);
                    }
                };
                //fbService.getLoginStatus();
                return backendService;
            }
        ]);
})();